// Vendors
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
// Components
import AppointmentCard from '../../components/AppointmentCard/AppointmentCard';
import AvailableDates from '../../components/AvailableDates/AvailableDates';

class Home extends Component {
  buildRedirect() {
    const { loginStatus: { status } } = this.props;
    let template = null;
    if(status === 'failed' || status === null) {
      template = (
        <Redirect to='/login' />
      );
    }
    return template;
  }
  render() {
    return (
      <div>
        <AppointmentCard />
        <hr />
        <AvailableDates />
        {this.buildRedirect()}
      </div>
    );
  }    
}

export default connect(
  state => ({
    loginStatus: state.usersReducer.login
  })
)(Home);