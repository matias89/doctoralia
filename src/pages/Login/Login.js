// Vendors
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
// Components
import InputText from '../../components/InputText/InputText';
// Actions
import userActions from '../../actions/userActions';

class Login extends Component {
  constructor(props) {
    super(props);
    this.buildMessage = this.buildMessage.bind(this);
    this.buildRedirect = this.buildRedirect.bind(this);
    this.handleOnLogin = this.handleOnLogin.bind(this);
  }
  handleOnLogin(event) {
    event.preventDefault();
    const { form: { user, password }, doLogin } = this.props;
    if(user && password) {
      doLogin(user, password);
    }
  }
  buildMessage() {
    const { loginStatus: { status, message } } = this.props;
    let template = null;
    if(status === 'failed') {
      template = (
        <div className="alert alert--error">
          <h5>{message}</h5>
        </div>
      );
    }
    return template;
  }
  buildRedirect() {
    const { loginStatus: { status } } = this.props;
    let template = null;
    if(status === 'success') {
      template = (
        <Redirect to='/' />
      );
    }
    return template;
  }
  render() {
    return (
      <div className="login">
        <form onSubmit={this.handleOnLogin}>
          <div className="form-group">
            <label>Usuario</label>
            <InputText
              placeholder="Ingrese su nombre de usuario aquí"
              id="user"
              type="text"
              required
            />
          </div>
          <div className="form-group">
            <label>Clave</label>
            <InputText
              placeholder="Ingrese su clave aquí"
              id="password"
              type="password"
              required
            />
          </div>
          <div className="form-group">
            <button className="button" type="submit">Ingresar</button>
          </div>
          {this.buildMessage()}
        </form>
        {this.buildRedirect()}
      </div>
    );
  }    
}

export default connect(
  state => ({
    form: state.formReducer,
    loginStatus: state.usersReducer.login
  }),{
    doLogin: userActions.doLogin
  }
)(Login);