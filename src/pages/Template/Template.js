// Vendors
import React from 'react';
// Components
import Header from '../../components/Header/Header';

const Template = props => {
return (
  <div>
    <Header />
    <div id="content">
      {props.children}
    </div>
    <footer>
      <div className="footer">
        <h6>www.doctoralia.es © 2019 - Encuentra tu especialista y pide cita.</h6>
      </div>
    </footer>
  </div>
  );
}

export default Template;