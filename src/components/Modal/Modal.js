// Vendors
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Styles
import './styles';

class Modal extends Component {
  render() {
    const { children, onClose, show, title } = this.props;
    return (
      <div className={`modal ${show ? 'show-modal' : ''}`}>
        <div className="modal-content">
            <span className="close-button" onClick={onClose}>&times;</span>
            <h3>{title}</h3>
            <hr />
            {children}
        </div>
    </div>
    );
  }
}
Modal.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func,
  show: PropTypes.bool,
  title: PropTypes.string
}

export default Modal;