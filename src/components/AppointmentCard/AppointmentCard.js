// Vendors
import React from 'react';
import { connect } from 'react-redux';
// Styles
import './styles.scss';
// Icons
import doctor from '../../assets/icons/doctor.png';
import calendar from '../../assets/icons/calendar.png';
import location from '../../assets/icons/location.png';

const AppointmentCard = props => {
  const {
    appointment: {
      mockedOriginalAppointment: {
        originalDate,
        professional,
        address
      }
    }
  } = props;
  return (
    <div className="appointment-card">
      <h2>Tu cita</h2>
      <h3>Detalles de tu reserva médica</h3>
      <div className="appointment-card__container">
        <div className="appointment-card__item">
          <div className="appointment-card__icon"><img src={doctor} style={{width: '100%'}} /></div>
          <div className="appointment-card__description">
            <h5>Profesional</h5>
            <h4>{professional}</h4>
          </div>
        </div>
        <div className="appointment-card__item">
          <div className="appointment-card__icon"><img src={calendar} style={{width: '100%'}} /></div>
          <div className="appointment-card__description">
            <h5>Fecha de la cita</h5>
            <h4>{originalDate}</h4>
          </div>
        </div>
        <div className="appointment-card__item">
          <div className="appointment-card__icon"><img src={location} style={{width: '100%'}} /></div>
          <div className="appointment-card__description">
            <h5>Lugar</h5>
            <h4>{address}</h4>
          </div>
        </div>
      </div>
    </div>
    );
}

export default connect(
  state => ({
    appointment: state.appointmentReducer
  })
)(AppointmentCard);