// Vendors
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// Actions
import formActions from '../../actions/formActions';

class InputText extends Component {
  constructor(props) {
    super(props);
    const { id } = props;
    this.handleOnChange = this.handleOnChange.bind(this);
    this.state = {
      [id]: ''
    }
  }
  handleOnChange(event) {
    const { target: { id, value }} = event;
    this.props.onChange(id, value);
  }
  render() {
    const { id, placeholder, type, form, required } = this.props;
    return (
      <div className="form-group">
        <input
          type={type}
          id={id}
          className="form-element"
          placeholder={placeholder}
          onChange={this.handleOnChange}
          value={form[id]}
          required={required}
        />
      </div>
    );
  }
}

InputText.propTypes = {
  form: PropTypes.object,
  id: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  type: PropTypes.string
}

export default connect(
  state => ({
    form: state.formReducer
  }), {
    onChange: formActions.onChange
  }
)(InputText);