// Vendors
import React from 'react';
// Components
import Calendar from '../Calendar/Calendar';

const AvailableDates = () => {
  return (
    <div className="available-dates">
        <h2>Cupos disponibles</h2>
        <h3>Si te ha surgido un problema para asistir, puedes reprogramar el turno seleccionando alguno de los disponibles en la lista.</h3>
        <Calendar />
    </div>
  );
}

export default AvailableDates;