// Vendors
import React from 'react';
// Images
import loader from '../../assets/icons/loader.gif';

const Loader = () => {
  return (
    <div style={{width: '100%', textAlign: 'center'}}>
      <img src={loader} alt="Cargando..." />
    </div>
  );
}

export default Loader;