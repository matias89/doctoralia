// Vendors
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
// Actions
import userActions from '../../actions/userActions';
// Icons
import logo from '../../assets/icons/logo.svg';
// Styles
import './styles.scss';

class Header extends Component {
  constructor(props) {
    super(props);
    this.handleOnLogout = this.handleOnLogout.bind(this);
  }
  handleOnLogout() {
    this.props.doLogout();
  }
  buildOptions(name) {
    return (
      <div className="header__options">
        Hola {name} - <button type="button" className="link" onClick={this.handleOnLogout}>Cerrar sesión</button>
      </div>
    );
  }
  render() {
    const { users: { userData: { name } } } = this.props;
    return (
      <header className="header">
        <div className="header__container">
          <img src={logo} width="100" />
          {name ? this.buildOptions(name) : null}
        </div>
      </header>
    );
  }
}

export default connect(
  state => ({
    users: state.usersReducer
  }), {
    doLogout: userActions.doLogout
  }
)(Header);