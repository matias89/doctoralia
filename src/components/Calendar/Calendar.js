// Vendors
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// Components
import Modal from '../Modal/Modal';
import InputText from '../InputText/InputText';
import CalendarColumn from './CalendarColumn';
// Actions
import calendarActions from '../../actions/calendarActions';
// Utils
import { formatedDate, localeDate } from '../../utils/date';
// Styles
import './styles';

class Calendar extends Component {
  constructor(props) {
    super(props);
    this.handleOnAccept = this.handleOnAccept.bind(this);
    this.handleOnClose = this.handleOnClose.bind(this);
    this.handleOnNextPage = this.handleOnNextPage.bind(this);
    this.handleOnPrevPage = this.handleOnPrevPage.bind(this);
    this.handleOnSelectDate = this.handleOnSelectDate.bind(this);
    this.state = {
      week: 0,
      showModal: false
    }
  }
  componentDidMount() {
    const today = new Date();
    this.getData(today);
  }
  componentWillUnmount() {
    // window.clearInterval(this.interval); // En caso de que se habilite el window.setInterval
  }
  getData(today) {
    const { getCalendar } = this.props;
    // Si today no es lunes, debo llamar dos veces al servicio para traer todos los datos
    const currentDay = today.getDay();
    if(currentDay === 0) { // Domingo
      today.setDate(today.getDate() + 1);
      const fDate = formatedDate(today);
      getCalendar(fDate);
    } else if(currentDay === 1) { // Lunes
      const fDate = formatedDate(today);
      getCalendar(fDate);
    } else { // Resto de la semana
      today.setDate(today.getDate() + 1 - currentDay);
      const fDate1 = formatedDate(today);
      today.setDate(today.getDate() + 7);
      const fDate2 = formatedDate(today);
      getCalendar(fDate1);
      getCalendar(fDate2);
    }
    this.interval = window.setInterval(() => {
      // this.getData(new Date()); // En caso que se quiera cada cierto tiempo revisar los slots disponibles
    }, 60000);
  }
  handleOnAccept() {
    const {
      calendar: { selectedDate },
      form: { problemDescription },
      user: { userData: {
        name,
        secondname,
        email,
        phone,
      }}
    } = this.props;
    this.props.createAppointment({
      name,
      secondname,
      email,
      phone,
      problemDescription,
      selectedDate
    });
  }
  handleOnClose() {
    this.props.resetAppointment();
    this.setState(() => ({
      showModal: false
    }));
  }
  handleOnNextPage() {
    const { week } = this.state;
    const today = new Date();
    const dWeek = today.getDate() > 1 ? 7 : 1;
    today.setDate(today.getDate() + (1 * dWeek) + ( week * 7 ));
    this.getData(today);
    this.setState(() => ({
      week: week + 1
    }))
  }
  handleOnPrevPage() {
    this.setState(() => ({
      week: this.state.week - 1
    }))
  }
  handleOnSelectDate(selectedDate) {
    const { selectDate } = this.props;
    this.setState(() => ({
      showModal: true
    }));
    selectDate(selectedDate);
  }
  buildColumns() {
    const { calendar } = this.props;
    return (
      <CalendarColumn
        onSelectDate={this.handleOnSelectDate}
        calendar={calendar}
        onTryAgain={() => {this.getData(new Date())}}
        week={this.state.week}
      />
    );
  }
  buildSuccessView() {
    const { calendar: { selectedDate: { Start } } } = this.props;
    return (
      <div className="message message--success">
        <h5>Modificación finalizada correctamente.<br />Su cita ha sido reprogramada para la siguiente fecha: {localeDate(Start)}</h5>
      </div>
    );
  }
  buildConfirmView() {
    const {
      calendar: { selectedDate },
      appointment: { loading, mockedOriginalAppointment: { originalDate } }
    } = this.props;
    return (
      <div>
        <h4>Fecha original</h4>
        <p>{originalDate}</p>
        <h4>Fecha nueva</h4>
        <p>{selectedDate ? localeDate(selectedDate.Start) : 'Error'}</p>
        <h4>Comentarios</h4>
        <InputText placeholder="describa brevemente su problema" id="problemDescription" type="text" />
        <p style={{textAlign: 'center', paddingTop: '20px'}}>
          <button
            className={`calendar__schedule-button ${loading ? 'calendar__schedule-button--disabled' : ''}`}
            type="button"
            disabled={loading}
            onClick={this.handleOnAccept}
          >
            {loading ? 'Espere ...' : 'Confirmar cambio'}
          </button>
        </p>
      </div>
    );
  }
  buildModalView() {
    const {
      appointment: { error, message }
    } = this.props;
    return (
      <Modal
        show={this.state.showModal}
        onClose={this.handleOnClose}
        title={error === false ? '¡Éxito!' : 'Confirmar nueva fecha'}
      >
        {error === null ? this.buildConfirmView() : null}
        {error === false ? this.buildSuccessView() : null}
        {error === true ? (
          <div className="message message--error">
            <h5>No se pudo realizar el cambio. {message}</h5>
          </div>
        ) : null}
      </Modal>
    );
  }
  render() {
    return (
      <div className="calendar">
      <div className="calendar__header">
        <div className="calendar__button-left">
        {this.state.week > 0 ? (
          <span className="calendar__link"  onClick={this.handleOnPrevPage}>Anterior</span>
        ) : null}
        </div>
        <div className="calendar__date-description"><h4>Fechas disponibles</h4></div>
        <div className="calendar__button-right">
          <span className="calendar__link" onClick={this.handleOnNextPage}>Siguiente</span>
        </div>
      </div>
      <div className="calendar__content">
        {this.buildColumns()}
      </div>
      {this.buildModalView()}
    </div>
    );
  }
}

Calendar.propTypes = {
  appointment: PropTypes.object,
  calendar: PropTypes.object,
  form: PropTypes.object,
  loading: PropTypes.bool,
  user: PropTypes.object
}

export default connect(
  state => ({
    calendar: state.calendarReducer,
    form: state.formReducer,
    user: state.usersReducer,
    appointment: state.appointmentReducer
  }), {
    getCalendar: calendarActions.getCalendar,
    selectDate: calendarActions.selectDate,
    createAppointment: calendarActions.createAppointment,
    resetAppointment: calendarActions.resetAppointment
  }
)(Calendar);