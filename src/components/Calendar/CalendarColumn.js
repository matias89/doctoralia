// Vendors
import React from 'react';
import PropTypes from 'prop-types';
// Components
import CalendarColumnItem from './CalendarColumnItem';
import Loader from '../Loader/Loader';
// Constants
import { ARRAY_OF_DAYS, ARRAY_OF_MONTHS } from '../../constants';
// Utils
import { formatedDate } from '../../utils/date';

const CalendarColumn = props => {
  const items = [];
  const { calendar: { slots, status }, onSelectDate, onTryAgain, week } = props;
  if(status === 'failed') {
    items[0] = (
      <div className="calendar__error" key="error">
        <h5>Ha ocurrido un error. Verifica tu conexión y vuelve a intentar.</h5>
        <span className="calendar__link" onClick={onTryAgain}>Reintentar</span>
      </div>
    );
  } else {
    if(slots.length === 0) {
      items[0] = <Loader key="loader" />;
    } else {
      for(let i = 0; i <= 6; i++) {
        const date = new Date();
        date.setDate(date.getDate() + i + (week * 7));
        const dateFormated = formatedDate(date);
        const availablesToday = slots[dateFormated];
        items[i] = (
          <div className={`calendar__column-date ${!availablesToday ? 'calendar__column-date--hide' : ''}`} key={`${i}_${dateFormated}`}>
            <div className="calendar__column-date-item">
              <div className="calendar__column-date-item--fixed">
                <h6>{ARRAY_OF_DAYS[date.getDay()]}</h6>
                <h5>{date.getDate()} {ARRAY_OF_MONTHS[date.getMonth()]}</h5>
                <hr />
              </div>
              <CalendarColumnItem
                availablesToday={availablesToday}
                onSelectDate={onSelectDate}
              />
            </div>
          </div>
        );
      }
    }
  }
  return items;
}

CalendarColumn.propTypes = {
  calendar: PropTypes.object,
  onSelectDate: PropTypes.func,
  onTryAgain: PropTypes.func,
  week: PropTypes.number
}

export default CalendarColumn;