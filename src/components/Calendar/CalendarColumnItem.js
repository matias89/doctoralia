// Vendors
import React from 'react';
import PropTypes from 'prop-types';

const CalendarColumnItem = props => {
  const { availablesToday, onSelectDate } = props;
  let template;
  const date1 = new Date();
  if(availablesToday) {
    template = availablesToday.map((item, index) => {
      const buttonProps = {
        className: 'calendar__schedule-button',
        onClick: () => {
          onSelectDate(item);
        },
        type: 'button',
        key: index // No es la mejor alternativa, el servicio debería traer un ID unico
      }
      // Ademas de taken, ver la hora actual tambien, para deshabiltar si la hora ya transcurrió
      // Y no dar la posibilidad de tomar un turno en un horario viejo
      const date2 = new Date(item.Start);
      if(date1 > date2 || (item.Taken && item.Taken === true)) {
        buttonProps.disabled = true;
        buttonProps.className = 'calendar__schedule-button calendar__schedule-button--disabled';
      }
      return (
        <button {...buttonProps}>
            {item.Start.substring(11, 16)}
        </button>
      );
    });
  } else {
    template = (
      <h5>No hay turnos disponibles para este día</h5>
    );
  }
  return template;
}

CalendarColumnItem.propTypes = {
  availablesToday: PropTypes.array,
  onSelectDate: PropTypes.func
}

export default CalendarColumnItem;
