// Vendors
import 'regenerator-runtime/runtime';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
// Reducers
import reducers from '../reducers'
// Sagas
import rootSaga from '../sagas';
// Utils
import { loadState, saveState } from '../utils/localStorage';

const sagaMiddleware = createSagaMiddleware();
const persistedState = loadState();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducers,
  persistedState,
  composeEnhancers(
    applyMiddleware(sagaMiddleware)
  )
);
store.subscribe(() => {
  saveState({
    usersReducer: store.getState().usersReducer
  })
});
sagaMiddleware.run(rootSaga)

export default store;