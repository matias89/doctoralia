import { USER_LOGIN_REQUEST, USER_LOGOUT_REQUEST } from '../constants';

const doLogin = (user, password) => {
  return {
    type: USER_LOGIN_REQUEST,
    payload: {
    user, password
    }
  }
}
const doLogout = () => {
  return {
    type: USER_LOGOUT_REQUEST
  }
}
  
export default {
doLogin,
doLogout
}