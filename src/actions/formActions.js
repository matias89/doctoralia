import { INPUT_ON_CHANGE } from '../constants';

const onChange = (id, value) => {
    return {
      type: INPUT_ON_CHANGE,
      payload: {
        id, value
      }
    }
  }
  
  export default {
    onChange
  }