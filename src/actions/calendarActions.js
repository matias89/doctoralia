// Constansts
import { GET_CALENDAR, SET_DATE, CREATE_APPOINTMENT, RESET_APPOINTMENT } from '../constants';

const getCalendar = date => {
  return {
    type: GET_CALENDAR,
    payload: {
      date
    }
  }
}

const selectDate = selectedDate => ({
  type: SET_DATE,
  payload: selectedDate
})

const createAppointment = selectedDate => ({
  type: CREATE_APPOINTMENT,
  payload: selectedDate
});

const resetAppointment = () => ({
  type: RESET_APPOINTMENT
});

export default {
  createAppointment,
  getCalendar,
  resetAppointment,
  selectDate
}