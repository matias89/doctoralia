// Vendors
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

// Pages
import Login from './pages/Login/Login';
import Home from './pages/Home/Home';

// Template
import Template from './pages/Template/Template';
// Store
import store from './store';

// Styles
import './main.scss';

const App = () => {
  return (
    <Provider store={store}>
      <Template>
      <BrowserRouter>
        <Route path='/login' exact component={Login} />
        <Route path='/' exact component={Home} />
      </BrowserRouter>
    </Template>
    </Provider>
  );
}
render(
  <App />,
  document.getElementById('app'),
);