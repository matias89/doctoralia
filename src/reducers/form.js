// Constants
import { INPUT_ON_CHANGE } from '../constants/index'

const initialState = {
  user: '',
  password: '',
  problemDescription: ''
};
  
const formReducer = (state = initialState, action) => {
  switch(action.type) {
    case INPUT_ON_CHANGE:
      const { payload: { id, value }} = action;
      return Object.assign({}, {
          ...state,
          [id]: value
      });
    default:
      return state;
  }
}

export default formReducer;