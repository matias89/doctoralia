// Constants
import { USER_LOGIN_SUCCESS, USER_LOGIN_FAILED, USER_LOGOUT_SUCCESS } from '../constants';

const initialState = {
  login: {
    status: null,
    message: null
  },
  userData: {}
};

const usersReducer = (state = initialState, action) => {
  switch(action.type) {
    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        login: {
          status: 'success',
          message: 'Conexión exitosa'
        },
        userData: {
          ...action.payload
        }
      };
    case USER_LOGIN_FAILED:
      return {
        ...state,
        login: {
          status: 'failed',
          message: action.payload.message
        }
      };
    case USER_LOGOUT_SUCCESS:
      return initialState;
    default:
      return state;
  }
}

export default usersReducer;