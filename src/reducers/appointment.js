// Constants
import {
  CREATE_APPOINTMENT,
  CREATE_APPOINTMENT_FAILED,
  CREATE_APPOINTMENT_SUCCESS,
  RESET_APPOINTMENT
} from '../constants';

const initialState = {
  loading: false,
  error: null,
  message: '',
  mockedOriginalAppointment: {
    address: 'Carrer Bac de Roda 83, Barcelona',
    professional: 'Doctor Nick Riviera',
    originalDate: (new Date(2019, 3, 20, 9, 0)).toLocaleString('es-ES')
  }
};
  
const appointmentReducer = (state = initialState, action) => {
  switch(action.type) {
    case CREATE_APPOINTMENT:
      return Object.assign({}, {
        ...state,
        error: null,
        loading: true,
        message: ''
      });
    case CREATE_APPOINTMENT_FAILED:
      return Object.assign({}, {
        ...state,
        error: true,
        loading: false,
        message: `Descripción del error: ${action.payload}`
      });
    case CREATE_APPOINTMENT_SUCCESS:
      return Object.assign({}, {
        ...state,
        error: false,
        loading: false
      });
    case RESET_APPOINTMENT:
      return initialState;
    default:
      return state;
  }
}

export default appointmentReducer;