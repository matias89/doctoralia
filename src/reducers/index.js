// Vendors
import { combineReducers } from 'redux'
// Reducers
import usersReducer from './users';
import calendarReducer from './calendar';
import formReducer from './form';
import appointmentReducer from './appointment';

export default combineReducers({
    calendarReducer,
    formReducer,
    usersReducer,
    appointmentReducer
});