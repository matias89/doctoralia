// Constants
import {
  APPOINTMENT_SELECTED,
  GET_CALENDAR_FAILED,
  GET_CALENDAR_SUCCESS,
  SET_DATE
} from '../constants';
// Utils
import { findAndDisable } from '../utils/filter';
import { formatedDate } from '../utils/date';

const initialState = {
    slots: [],
    selectedDate: null,
    status: null
  };
  
  const calendarReducer = (state = initialState, action) => {
    switch(action.type) {
      case GET_CALENDAR_SUCCESS:
        return Object.assign({}, {
          ...state,
          status: null,
          slots: {
            ...state.slots,
            ...action.payload
          }
        });
      case GET_CALENDAR_FAILED:
        return Object.assign({}, {
          ...state,
          status: 'failed'
        });
      case SET_DATE:
        return Object.assign({}, {
          ...state,
          selectedDate: action.payload
        });
      case APPOINTMENT_SELECTED:
        // Find slot and disable it
        const { selectedDate: { Start }, slots } = state;
        const fDate = formatedDate(new Date(Start));
        const uSlots = findAndDisable(Start, slots[fDate]);
        console.log('updatedSlots', uSlots);
        return Object.assign({}, {
          ...state,
          slots: {
            ...state.slots,
            [slots[fDate]]: uSlots
          }
        })
      default:
        return state;
    }
  }
  
  export default calendarReducer;