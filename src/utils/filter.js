const findAndDisable = (selected, list) => {
  let flag = 0;
  return list.map(item => {
    if(selected === item.Start || flag !== 0) {
      item.Taken = true;
      flag++;
      if(flag === 6) {
        flag = 0;
      }
    }
    return item;
  });
}

export {
  findAndDisable
}