const formatDate = date => {
  return date < 10 ? `0${date}` : date;
}

const formatedDate = date => {
  const day = formatDate(date.getDate());
  const month = formatDate(date.getMonth() + 1);
  const year = date.getFullYear();
  return `${year}${month}${day}`;
}

const localeDate = date => {
  return (new Date(date)).toLocaleString('es-ES');
}

export {
  formatedDate,
  localeDate
}