// Constants
import { API_URL } from '../constants';

const apiPost = (endpoint, body) => {
    let url = `${API_URL}/${endpoint}`;
    if(endpoint === 'auth') {
        url = 'http://localhost:3000/auth/login';
    }
    return window.fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify({...body})
    });
}

const apiGet = endpoint =>   {
    return window.fetch(`${API_URL}/${endpoint}`, {
        method: 'get',
        headers: {
            'Content-Type': undefined
        }
    });
}

export {
    apiGet,
    apiPost
}