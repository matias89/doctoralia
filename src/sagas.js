// Vendors
import { all, call, put, takeEvery, takeLatest } from 'redux-saga/effects';
// Services
import { apiGet, apiPost } from './services/api';
// Utils
import { clearData } from './utils/localStorage';
// Constants
import {
  APPOINTMENT_SELECTED,
  CREATE_APPOINTMENT,
  CREATE_APPOINTMENT_FAILED,
  CREATE_APPOINTMENT_SUCCESS,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILED,
  USER_LOGOUT_REQUEST,
  USER_LOGOUT_SUCCESS,
  GET_CALENDAR,
  GET_CALENDAR_SUCCESS,
  GET_CALENDAR_FAILED
} from './constants';

function* getCalendar(action) {
  const { payload: { date } } = action;
  try {
    const data = yield call(apiGet, `availability/GetWeeklySlots/${date}`);
    const slots = yield data.json();
    const elements = {};
    slots.forEach(element => {
      const index = element.Start.substring(0, 10).replace(/-/g, '');
      if(!elements[index]) {
        elements[index] = [];
      }
      elements[index].push(element);
    });
    yield put({type: GET_CALENDAR_SUCCESS, payload: elements});
  } catch (error) {
    yield put({type: GET_CALENDAR_FAILED, message: error.message});
  }
}

function* createAppointment(action) {
  try {
    const { payload:
      { selectedDate: { Start },
      problemDescription,
      name,
      secondname,
      phone,
      email
    }} = action;
    const date = new Date(Start);
    date.setHours(date.getHours() + 1); // Turnos de una hora
    const End = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 ))
    .toISOString();
    const data = yield call(apiPost, 'availability/BookSlot', {
      Start,
      End,
      Comments: problemDescription,
      Patient: {
        Name: name,
        //Secondname: secondname,
        Phone: phone,
        Email: email
      }
    });
    if(data.status === 200) {
      yield put({type: CREATE_APPOINTMENT_SUCCESS});
      yield put({type: APPOINTMENT_SELECTED});
    } else {
      const response = yield data.json();
      yield put({type: CREATE_APPOINTMENT_FAILED, payload: response});
    }
  } catch (error) {
    yield put({type: CREATE_APPOINTMENT_FAILED, payload: error.message});
  }
}

function* userLogin(action) {
  try {
    const { payload: { user, password }} = action;
    const data = yield call(apiPost, 'auth', {
      email: user, password
    });
    const userData = yield data.json();
    if(userData.access_token) {
      yield put({type: USER_LOGIN_SUCCESS, payload: userData});
    } else {
        yield put({type: USER_LOGIN_FAILED, payload: userData});
    }
  } catch (error) {
    yield put({type: USER_LOGIN_FAILED, payload: {
      status: 401,
      message: error.message
    }});
  }
}
function* userLogout() {
  clearData();
  yield put({type: USER_LOGOUT_SUCCESS});
}

function* rootSaga() {
  yield all([
    takeEvery(GET_CALENDAR, getCalendar),
    takeEvery(CREATE_APPOINTMENT, createAppointment),
    takeEvery(USER_LOGIN_REQUEST, userLogin),
    takeEvery(USER_LOGOUT_REQUEST, userLogout)
  ]);
}

export default rootSaga;