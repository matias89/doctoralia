# Doctoralia
## Proyecto
Calendario para re programación de turnos médicos.

## Instalación
`npm install`
## Correr servidor
`npm run doctoralia`
## Ver la aplicación
http://localhost:1234
## Datos de prueba
- matias.aybar@gmail.com / matias1234
- begona.ortiz@docplanner.com / begona1234
- laura.pujol@docplanner.com / laura1234

Los usuarios pueden agregarse o quitarse en el archivo `users.json`