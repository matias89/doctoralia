const fs = require('fs');
const bodyParser = require('body-parser');
const jsonServer = require('json-server');
const jwt = require('jsonwebtoken');

// Create server
const server = jsonServer.create();
// Router
const router = jsonServer.router('./db.json'); // En caso de tener datos mockeados que no sean los de usuario
// Users
const userdb = JSON.parse(fs.readFileSync('./users.json', 'UTF-8'));
// Config
server.use(jsonServer.defaults());
server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());
const SECRET_KEY = '987654321';
const expiresIn = '1h';
// Create a token from a payload 
function createToken(payload){
  return jwt.sign(payload, SECRET_KEY, {expiresIn});
}

// Verify the token 
function verifyToken(token){
  return jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ?  decode : err);
}
  
// Check if the user exists in database
function isAuthenticated({email, password}){
  const user = userdb.users.find(user => {
    if(user.email === email && user.password === password) {
      return {
        name: user.name,
        secondname: user.secondname,
        email,
        phone: user.phone
      };
    }
  });
  return user !== undefined ? user : false;
}
// POST
server.post('/auth/login', (req, res) => {
  const {email, password} = req.body;
  const userData = isAuthenticated({email, password});
  if (userData === false) {
    const status = 401;
    const message = 'Los datos ingresados son incorrectos. Por favor, verifique y vuelva a intentar.';
    res.status(status).json({status, message});
    return;
  }
  const access_token = createToken({email, password});
  res.status(200).json({access_token, ...userData});
});
// Express middleware
server.use(/^(?!\/auth).*$/,  (req, res, next) => {
  if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
    const status = 401;
    const message = 'Bad authorization header';
    res.status(status).json({status, message});
    return;
  }
  try {
     verifyToken(req.headers.authorization.split(' ')[1]);
     next();
  } catch (err) {
    const status = 401;
    const message = 'Error: access_token no es válido';
    res.status(status).json({status, message});
  }
})
// Mount server
server.use('api', router);
server.listen(3000, () => {
  console.log('Run Auth API Server')
})
